var hraciaDoska;
const hrac = 'O';
const pocitac = 'X';
const vyherneKombinacie = [
    [0, 1, 2],
    [3, 4, 5],
    [6, 7, 8],
    [0, 3, 6],
    [1, 4, 7],
    [2, 5, 8],
    [0, 4, 8],
    [6, 4, 2]
]

const stlpce = document.querySelectorAll('.policko');
start();

function start() {
    document.querySelector(".koniecHry").style.display = "none";
    hraciaDoska = Array.from(Array(9).keys());
    for (var i = 0; i < stlpce.length; i++) {
        stlpce[i].innerText = '';
        stlpce[i].style.removeProperty('background-color');
        stlpce[i].addEventListener('click', turnClick, false);
    }
}

function turnClick(square) {
    if (typeof hraciaDoska[square.target.id] == 'number') {
        turn(square.target.id, hrac)
        if (!kontrolaVyhry(hraciaDoska, hrac) && !kontrolaRemiza()) turn(najMiesto(), pocitac);
    }
}

function turn(squareId, hrac) {
    hraciaDoska[squareId] = hrac;
    document.getElementById(squareId).innerText = hrac;
    let vyhrataHra = kontrolaVyhry(hraciaDoska, hrac)
    if (vyhrataHra) koniecHry(vyhrataHra)
}

function kontrolaVyhry(board, hrac) {
    let plays = board.reduce((a, e, i) =>
        (e === hrac) ? a.concat(i) : a, []);
    let gameWon = null;
    for (let [index, win] of vyherneKombinacie.entries()) {
        if (win.every(elem => plays.indexOf(elem) > -1)) {
            gameWon = {index: index, hrac: hrac};
            break;
        }
    }
    return gameWon;
}

function koniecHry(vysledokHry) {
    for (let index of vyherneKombinacie[vysledokHry.index]) {
        document.getElementById(index).style.backgroundColor =
            vysledokHry.hrac == hrac ? "blue" : "red";
    }
    for (var i = 0; i < stlpce.length; i++) {
        stlpce[i].removeEventListener('click', turnClick, false);
    }
    spracovanieVyhry();
}



function spracovanieVyhry(){
   document.getElementById("formWin").submit();

}

function prazdnePolicka() {
    return hraciaDoska.filter(s => typeof s == 'number');
}


function najMiesto() {
    return minimax(hraciaDoska, pocitac).index;
}

function kontrolaRemiza() {
    if (prazdnePolicka().length == 0) {
        for (var i = 0; i < stlpce.length; i++) {
            stlpce[i].style.backgroundColor = "green";
            stlpce[i].removeEventListener('click', turnClick, false);
        }
        urciVitaza("Remíza!")
        return true;
    }
    return false;
}

function minimax(doskaNova, hrac) {
    var dostupneMiesta = prazdnePolicka();

    if (kontrolaVyhry(doskaNova, hrac)) {
        return {score: -10};
    } else if (kontrolaVyhry(doskaNova, pocitac)) {
        return {score: 10};
    } else if (dostupneMiesta.length === 0) {
        return {score: 0};
    }
    var moves = [];
    for (var i = 0; i < dostupneMiesta.length; i++) {
        var move = {};
        move.index = doskaNova[dostupneMiesta[i]];
        doskaNova[dostupneMiesta[i]] = hrac;

        if (hrac == pocitac) {
            var vysledok = minimax(doskaNova, hrac);
            move.score = vysledok.score;
        } else {
            var vysledok = minimax(doskaNova, pocitac);
            move.score = vysledok.score;
        }

        doskaNova[dostupneMiesta[i]] = move.index;

        moves.push(move);
    }

    var najTah;
    if(hrac === pocitac) {
        var maxSkore = -10000;
        for(var i = 0; i < moves.length; i++) {
            if (moves[i].score > maxSkore) {
                maxSkore = moves[i].score;
                najTah = i;
            }
        }
    } else {
        var maxSkore = 10000;
        for(var i = 0; i < moves.length; i++) {
            if (moves[i].score < maxSkore) {
                maxSkore = moves[i].score;
                najTah = i;
            }
        }
    }

    return moves[najTah];

}
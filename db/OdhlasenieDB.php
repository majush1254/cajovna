<?php

if (isset($_POST['submit'])) {
    session_start();
    session_destroy();
    header("Location: ../core/index.php?stranka=prihlasenie");
    exit();
}
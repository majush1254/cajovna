<?php

session_start();    //aby fungovali premenne zo sessionu

if (isset($_POST['submit'])) {
    include 'specDB.php';

    $login = mysqli_real_escape_string($pripojenie, $_POST['login']); //nemoze byt prikaz
    $psw = mysqli_real_escape_string($pripojenie, $_POST['psw']); // spracovanie stringov, oreže medzery a podobne

    if(empty($login) || empty($psw)) {
        header("Location: ../core/index.php?stranka=prihlasenie&login=empty");
        exit();
    } else {
        $vyraz = "SELECT * FROM pouzivatelia WHERE login='$login'";
        $vysledky = mysqli_query($pripojenie, $vyraz);
        $kontrolaVysledkov = mysqli_num_rows($vysledky);
        //echo "$kontrolaVysledkov";
        if ($kontrolaVysledkov < 1){
            header("Location: ../core/index.php?stranka=prihlasenie&login=chybnyLogin");
            exit();
        } else {
            if ($riadok = mysqli_fetch_assoc($vysledky)) {

                $zasifrovanieHesla = password_verify($psw, $riadok['psw']);
                if (false == $zasifrovanieHesla) {
                    header("Location: ../core/index.php?stranka=prihlasenie&login=chybneHeslo");
                    exit();
                } elseif (true==$zasifrovanieHesla) {
                    $_SESSION['login'] = $riadok['login'];
                    $_SESSION['psw'] = $riadok['psw'];
                    $_SESSION['vek'] = $riadok['vek'];
                    $vyraz = "SELECT * FROM odmeny WHERE pouz_login='$login'"; //uz to hral
                    $vysledky = mysqli_query($pripojenie, $vyraz);
                    $kontrolaVysledkov = mysqli_num_rows($vysledky);
                    $riadok = mysqli_fetch_assoc($vysledky);
                    if ($kontrolaVysledkov == 1){
                        $_SESSION['uzHral'] = 1;
                        $_SESSION['hraDatum'] = $riadok['odmena_datum'];
                    }else{
                        $_SESSION['uzHral'] = 0;
                        $_SESSION['hraDatum'] = null;
                    }
                    header("Location: ../core/index.php?stranka=pouzivatelskeInformacie&login=uspesne");

                    exit();
                }
            }
        }
    }
} else {
    header("Location: ../core/index.php?stranka=prihlasenie&login=chyba");
    exit();
}
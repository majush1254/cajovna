<?php

session_start();
class DB
{
    public function __construct()
    {
        $this->db = new mysqli('localhost','root','heslo', 'cajovna');
        $this->db->set_charset("utf8");
    }
    public function getPonuka()
    {
        $ponuky = [];
        $vyraz = "SELECT * FROM `napojovylistok`";
        $vysledky = $this->db->query($vyraz);
        if ($vysledky->num_rows > 0){
            while($riadok = $vysledky->fetch_object()){
                $ponuky[] = new Ponuka($riadok->Nazov, $riadok->Cena);
            }
        }
        return $ponuky;
    }


}
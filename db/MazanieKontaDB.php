<?php
session_start();
include 'specDB.php';

if (isset($_POST['submit'])) {
    $login=$_SESSION['login'];
    $psw = mysqli_real_escape_string($pripojenie, $_POST['psw']);

    if (!empty($psw)) {
        $vyraz = "SELECT * FROM pouzivatelia WHERE login='$login'";
        $vysledok = mysqli_query($pripojenie, $vyraz);
        $riadok = mysqli_fetch_assoc($vysledok);
        $zasifrovanieHesla = password_verify($psw, $riadok['psw']);
        if($zasifrovanieHesla != false) {
            $vyraz = "DELETE FROM `pouzivatelia` WHERE login= '$login'";
            mysqli_query($pripojenie, $vyraz);
            session_destroy();
            header("Location: ../core/index.php?stranka=prihlasenie&zmazanie=uspech");
            exit();
        } else {
            header("Location: ../core/index.php?stranka=zmazatUcet&zmazanie=chyba");
            exit();
        }
    } else {
        header("Location: ../core/index.php?stranka=zmazatUcet&zmazanie=prazdneHeslo");
        exit();
    }

} else {
    header("Location: ../core/index.php?stranka=onas");
    exit();
}

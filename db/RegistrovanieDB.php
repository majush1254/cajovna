<?php
if (isset($_POST['submit'])) {
    include_once 'specDB.php';
    $login = mysqli_real_escape_string($pripojenie, $_POST['login']);
    $psw = mysqli_real_escape_string($pripojenie, $_POST['psw']);
    $vek = mysqli_real_escape_string($pripojenie, $_POST['vek']);
    if (empty($login) ||empty($vek)|| empty($psw)) {
        header("Location: ../core/index.php?stranka=registracia&registracia=prazdnaKolonka");
        exit();
    } else {
        if (!preg_match("/^[0-9]*$/", $vek) ) { // kontrola formularoveho prvku - vek musi mat iba ciselne znaky
            header("Location: ../core/index.php?stranka=registracia&registracia=neplatna");
            exit();
        } else {
            $vyraz = "SELECT * FROM pouzivatelia WHERE login='$login'";
            $vysledok = mysqli_query($pripojenie, $vyraz); // aplikujem query na databazku
            $kontrolaVysledkov = mysqli_num_rows($vysledok);
            if ($kontrolaVysledkov > 0) { // existuje rovnaky login
               header("Location: ../core/index.php?stranka=registracia&registracia=existujuciUzivatel");
                //echo "<span class='status-not-available'> Username Not Available.</span>";
                exit();
            } else {
                $zasifrovanieHesla = password_hash($psw, PASSWORD_DEFAULT); // zahashujem heslo
                $vyraz = "INSERT INTO pouzivatelia (login, psw, vek) VALUES ('$login','$zasifrovanieHesla', '$vek');"; // vytvorenie entity pouzivatela
                mysqli_query($pripojenie, $vyraz); //vykonam query
                header("Location: ../core/index.php?stranka=pouzivatelskeInformacie&registracia=uspesna");
                exit();
            }
        }
    }
} else {
    header("Location: ../core/index.php?stranka=registracia");
    exit();
}
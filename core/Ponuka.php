<?php

class Ponuka
{
    public $Nazov;
    public $Cena;
    public function __construct($Nazov, $Cena)
    {
        $this->Nazov = $Nazov;
        $this->Cena = $Cena;
    }

    public function getNazov()
    {
        return $this->Nazov;
    }

    public function setNazov($Nazov)
    {
        $this->Nazov = $Nazov;
    }

    public function getCena()
    {
        return $this->Cena;
    }

    public function setCena($Cena)
    {
        $this->Cena = $Cena;
    }


}
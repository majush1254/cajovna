<?php
include "Aplikacia.php";
include "../db/DB.php";
include "Ponuka.php";

$aplikacia=new Aplikacia();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <link rel="stylesheet" type="text/css" href="../styly.css">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta charset="UTF-8">
    <title>Cajovna</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>



</head>
<body>
<div id="hornyBbar" class="bar">
    <ul>
        <li><a href="?stranka=onas">O nás</a></li>
        <li><a href="?stranka=galeria">Galéria</a></li>
        <li><a href="?stranka=napojovyListok">Nápojový Lístok</a></li>
        <li><a href="?stranka=kontaktneUdaje">Kontaktné údaje</a></li>
        <li class="pe" style="visibility:visible"><a href="?stranka=registracia">Registrácia</a></li>
        <li class="pe" style="visibility:visible"> <a href="?stranka=prihlasenie">Prihlásenie</a></li>
        <li class="pi" style="visibility:hidden"> <a href="?stranka=pouzivatelskeInformacie">Používateľské informácie</a></li>
        <li class="pi" style="visibility:hidden"> <a href="?stranka=minihra">Mini hra</a></li>
        </ul>

    <?php
    if(isset($_SESSION['login'])){
    echo "<script>

        function odkry() {

            var x = document.getElementsByClassName('pi');
            var y = document.getElementsByClassName('pe');
            for(var i=0; i<x.length; i++) {
                if (x[i].style.visibility === 'hidden') {
                    x[i].style.visibility = 'visible';
                } else {
                    x[i].style.visibility = 'hidden';
                }
            }
            for(var i=0; i<y.length; i++) {
                if (y[i].style.visibility === 'hidden') {
                    y[i].style.visibility = 'visible';
                } else {
                    y[i].style.visibility = 'hidden';
                }
            }

        }
        odkry();
    </script>";
    } ?>

</div>
<div id="hlavnyKontajner">
    <?php switch ($aplikacia->aktualnaStranka) {
        case 0:
            include_once 'ONas.php';
            break;
        case 1:
            include_once 'Prihlasovanie.php';
            break;
        case 2:
            include_once 'NapojovyListok.php';
            break;
        case 3:
            include_once 'Registrovanie.php';
            break;
        case 4:
            include_once 'PouzivatelskeInformacie.php';
            break;
        case 5:
            include_once 'ZmenaPouzivatelskychInfo.php';
            break;
        case 6:
            include_once 'MazanieKonta.php';
            break;
        case 7:
            include_once 'Galeria.php';
            break;
        case 8:
            include_once 'KontaktneUdaje.php';
            break;
        case 9:
            include_once 'piskvorky.php';
            break;
        case 10:
            include_once 'odmietnutieHry.php';
            break;

    }?>
</div>
</body>
</html>
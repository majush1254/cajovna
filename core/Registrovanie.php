<?php
if(isset($_SESSION['login'])) { //blbuvzdornost
    header("Location: ../core/index.php?stranka=pouzivatelskeInformacie");
    exit();
}
?>
<div id="hlavna">
    <h2>Registrácia</h2>
    <p>Výpíš nasledujúce položky na vytvorenie konta.</p>
    <hr>
    <form class="myForm" action="../db/RegistrovanieDB.php" method="POST">
    <label><b>Login</b></label>
    <input type="text" placeholder="Zadaj login" name="login" id="login" required>

    <label for="psw"><b>Heslo</b></label>
    <input type="password" placeholder="Zadaj heslo" name="psw" id="psw" required>

    <label for="vek"><b>Vek</b></label>
    <input type="text" placeholder="Zadaj svoj vek" name="vek" id="vek" required>
        <?php
        if (isset($_GET['registracia'])){
            if ($_GET['registracia'] == 'prazdna') {
                echo '<p id="error">Je potrebné vyplniť všetky príslušné polia.</p>';
            } elseif ($_GET['registracia'] == 'neplatna') {
                echo '<p id="error">V kolonke vek nie sú podporované písmená. Zadaj iba čísla. </p>';
            } elseif ($_GET['registracia'] == 'existujuciPouzivatel') {
                echo '<p id="error">Daný používateľ existuje. Zvoľ si iný login alebo sa prihlás.</p>';
            } else {
                echo '<p id="error">chyba!</p>';
            }
        }
        ?>
    <hr>
        <p>Vytvorením tohto účtu súhlasíš s podmienkami registrácie.</p>

    <button type="submit" class="registerbtn" name="submit">Registrovať</button>
    </form>

</div>
<div class="container signin">
    <p>Máte už vytvorené konto? <a href="?stranka=prihlasenie">Prihlásiť sa</a>.</p>
</div>

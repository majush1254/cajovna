<?php

class Aplikacia // hlavny objekt celej aplikacie
{
    public $aktualnaStranka = 0;
    private $ponuka;
    public function __construct()
    {
      $this->ponuka = new DB();
        if (isset($_GET['stranka'])){
            if ($_GET['stranka'] == 'onas')
                $this->aktualnaStranka = 0;
            elseif ($_GET['stranka'] == 'prihlasenie')
                $this->aktualnaStranka = 1;
            elseif ($_GET['stranka'] == 'napojovyListok')
                $this->aktualnaStranka = 2;
            elseif ($_GET['stranka'] == 'registracia')
                $this->aktualnaStranka = 3;
            elseif ($_GET['stranka'] == 'pouzivatelskeInformacie')
                $this->aktualnaStranka = 4;
            elseif ($_GET['stranka'] == 'zmenaDat')
                $this->aktualnaStranka = 5;
            elseif ($_GET['stranka'] == 'zmazatUcet')
                $this->aktualnaStranka = 6;
            elseif ($_GET['stranka'] == 'galeria')
                $this->aktualnaStranka = 7;
            elseif ($_GET['stranka'] == 'kontaktneUdaje')
                $this->aktualnaStranka = 8;
            elseif ($_GET['stranka'] == 'minihra')
                $this->aktualnaStranka = 9;
            elseif ($_GET['stranka'] == 'odmietnutieHry')
                $this->aktualnaStranka = 10;
        }
    }
    public function getPonuky()
    {
        return $this->ponuka->getPonuka();
    }

}
<?php ?>

<div id="hlavna">
    <table id="ponukaTabulka">
        <th class="nazovStlpca1">Názov</th>
        <th class="nazovStlpca2">Cena</th>
        <?php foreach ($aplikacia->getPonuky() as $polozka) { ?>
            <tr>
                <th class="nazovStlpca1"><?php echo $polozka->getNazov()?></th>
                <th class="nazovStlpca2"><?php echo $polozka->getCena() . " €" ?></th>
            </tr>
        <?php } ?>
    </table>
</div>

<?php ?>
<div id="hlavna">
    <h1>O nás</h1>
    <hr class="style-seven">
    <p>Sme čajovňa, kde nájdete okrem dobrého čaju aj relax a oddych. V štýlovom prostredí našej čajovne ponúkame tie najkvalitnejšie a najlepšie čaje od overených dodávateľov z najslávnejších čajových oblastí sveta. </p>
    <p>Našim cieľom je distribúcia a predaj tých najkvalitnejších čajov z celého sveta.</p>
    <p>Naša predajňa je otvorená od Októbra 2015. Spoločnosť pri čaji vám budú robiť naše mačky, ktoré sa voľne pohybujú v priestoroch čajovne.   </p>
    <p>Je dokázané, že mačky sú výborné liečiteľky. Znižujú stres, pomáhajú pri depresiách a iných psychických problémoch. Atmosféru našej predajne dotvárajú dekorácie z rôznych kútov sveta. </p>
    <p><i>Dúfame, že sa Vám naša čajovňa  bude páčiť a prajeme príjemné chvíľky strávené pri čajíku v príjemnom prostredí našej čajovne.</i></p>
</div>


<img class="welcomeimg" src="../.idea/welcomeCat.jpg" alt="cat">

